def addOneTonumbers(numbers):
    carry = 1
    numbers = numbers + [1]
    while 1:
        if numbers[0] == 0:
            numbers.pop(0)
        else:
            break
    numbers = numbers[:-1]
    index = len(numbers)
    for number in numbers[::-1]:
        index -= 1
        numbers[index] = (carry + number) % 10
        carry = (carry + number) // 10
    if carry > 0:
        numbers = [carry] + numbers
    return numbers

if __name__ == "__main__":
    print(addOneTonumbers([0, 0, 9, 9]))