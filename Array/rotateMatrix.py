#rotate given matrix 90 degrees clockwise

def transposeMatrix(matrix):
    return [[matrix[j][i] for j in range(len(matrix))] for i in range(len(matrix[0]))]

def rotateMatrix(matrix):
    matrix =  transposeMatrix(matrix)
    matrix = [row[::-1] for row in matrix]
    return matrix

matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
print(rotateMatrix(matrix))


