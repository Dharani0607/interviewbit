#O(n) solution

def maxSubArraySum(array):
    maxTillNow = 0
    currentMax = 0
    for num in array:
        currentMax = max(num, currentMax+num)
        maxTillNow = max(currentMax, maxTillNow)
    return maxTillNow


array = [-2, -5, 6, 0, -2, 0, -3, 1, 0, 5, -6]
print(maxSubArraySum(array))
