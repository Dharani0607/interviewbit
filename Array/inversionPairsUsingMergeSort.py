def merge(arr, left, right):
    inversionPairs = 0
    i, j, k = 0, 0, 0
    while i < len(left) and j < len(right):
        if left[i] < right[j]:
            inversionPairs += j
            arr[k] = left[i]
            k, i = k+1, i+1
        else:
            arr[k] = right[j]
            k, j = k+1, j+1

    while i < len(left):
        inversionPairs += j
        arr[k] = left[i]
        k, i = k+1, i+1
    while j < len(right):
        arr[k] = right[j]
        k, j = k+1, j+1
    return inversionPairs

def mergeSort(array):
    inversionPairs = 0 
    if len(array) > 1:
        mid = len(array)//2
        left, right = array[:mid], array[mid:]
        inversionPairs += mergeSort(left)
        inversionPairs += mergeSort(right)
        inversionPairs += merge(array, left, right)
    return inversionPairs

array = [1, 20, 6, 4, 5, 3, 9, 10, 15] 
print(mergeSort(array))
